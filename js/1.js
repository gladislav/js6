// 1.Екранування - це коли мова програмування обмежує доступ до деяких змінних або функцій від інших
//  частин програми.
// 2.Оголошення функції за допомогою ключового слова function,Оголошення функції за допомогою змінної,
//   Оголошення функції за допомогою стрілкових функцій
// 3.Hoisting - це механізм, який використовується в JavaScript для того, щоб забезпечити можливість 
//   використання змінних та функцій до їх оголошення.

function createNewUser() {
  let firstName = prompt("Enter your first name:");
  let lastName = prompt("Enter your last name:");
  let birthdayStr = prompt("Введіть дату народження у форматі dd.mm.yyyy:");
  let [day, month, year] = birthdayStr.split(".");
  let birthday = new Date(year, month, day);
  let newUser = {
    firstName,
    lastName, 
    birthday,
    getLogin: function() {
      return (this.firstName.charAt(0) + this.lastName).toLowerCase();
    },
    getAge() {
      let now = new Date();
      let ageDiffMs = now - this.birthday.getTime();
      let ageDate = new Date(ageDiffMs);
      return Math.abs(ageDate.getUTCFullYear() - 1970);
    },
    
    getPassword() {
      return this.firstName.charAt(0).toUpperCase() +
        this.lastName.toLowerCase() +
        this.birthday.getFullYear();
    }
  };
  return newUser;
  }; 
let user = createNewUser();
console.log(user);
console.log(user.getLogin());
console.log("Вік:", user.getAge());
console.log("Пароль:", user.getPassword());

